import React, {Fragment, Component } from 'react';
import Button from "../../components/Button/Button";
import Card from "../../components/Card/Card";

class Blog extends Component {
  URL = 'https://api.chucknorris.io/jokes/random';

  state = {
    posts: [],
    count: 3
  };

  getPosts = () => {
    let array = [];
    for (let i = 0; i < this.state.count; i++) {
      array.push(fetch(this.URL).then(response => {
        if (response.ok) {
          return response.json();
        }
        throw new Error('Something wrong');
      }))
    }
    return Promise.all(array).then(posts => {
      const newPosts = posts.map(item => {
        return  {id: item.id, text: item.value};
      });
      this.setState({posts: newPosts});
    });
  };

  handlerGetCount = (event) => {
    let newCount = event.target.value;
    this.setState({count: newCount});
  };

  componentDidMount() {
    this.getPosts();
  }

  render() {
    return(
      <Fragment>
        <Button clicked={this.getPosts} change={this.handlerGetCount}>Get</Button>
        <Card posts={this.state.posts}/>
      </Fragment>
    )
  }
}

export default Blog;