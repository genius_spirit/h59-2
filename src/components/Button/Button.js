import React, { Component } from 'react';
import './Button.css';
import FormControl from "material-ui/es/Form/FormControl";
import InputLabel from "material-ui/es/Input/InputLabel";
import Input from "material-ui/es/Input/Input";

class Button extends Component {

  shouldComponentUpdate() {
    return false;
  }

  render() {
    return(
      <div className='wrapper'>
        <FormControl>
          <InputLabel htmlFor="name-input">Count of posts</InputLabel>
          <Input id="name-input" onChange={(event) => this.props.change(event)}/>
        </FormControl>
        <button onClick={this.props.clicked} className='btn'>Get posts</button>
      </div>
    )
  }
}

export default Button;