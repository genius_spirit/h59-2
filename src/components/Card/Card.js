import React from 'react';
import './Card.css';
import Paper from "material-ui/es/Paper/Paper";
import Typography from "material-ui/es/Typography/Typography";

const Card = ({posts}) => {
  return(
    <div className="Card-desk">
      {
        posts.map(item => {
         return (
             <Paper className='paper' key={item.id}>
               <Typography component="p" >
                 {item.text}
               </Typography>
             </Paper>
           )
          }
        )
      }
    </div>
  )
};

export default Card;